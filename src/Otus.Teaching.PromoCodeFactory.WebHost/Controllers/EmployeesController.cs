﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Create employee
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployeeAsync(EmployeeCreateRequest employeeCreateRequest)
        {
            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeCreateRequest.FirstName,
                LastName = employeeCreateRequest.LastName,
                Email = employeeCreateRequest.Email
            };

            await _employeeRepository.AddAsync(employee);

            var employeeModel = new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
            };

            Response.StatusCode = StatusCodes.Status201Created;

            return employeeModel;
        }

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task DeleteEmployeeAsync(Guid id)
        {
            await _employeeRepository.RemoveAsync(id);

            Response.StatusCode = StatusCodes.Status204NoContent;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles?.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Update employee
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task UpdateEmployeeAsync(EmployeeUpdateRequest employeeUpdateRequest)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeUpdateRequest.Id);

            if (employee == null)
                return;

            employee.FirstName = employeeUpdateRequest.FirstName;
            employee.LastName = employeeUpdateRequest.LastName;
            employee.Email = employeeUpdateRequest.Email;

            Response.StatusCode = StatusCodes.Status204NoContent;
        }
    }
}